package com.misl.umsys.chartofaccount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Nayeem on 2/25/2018.
 */
@RestController
@RequestMapping("/glaccount")
public class GlAccountController {

    @Autowired
    private final GlAccountService glAccountService;

    @Autowired
    public GlAccountController(GlAccountService glAccountService){
        this.glAccountService = glAccountService;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
     public HttpEntity<?> createGlAccount(@RequestBody GlAccount glAccount) throws Exception {
        try {
            Boolean result = glAccountService.createGlAccount(glAccount);
            return new ResponseEntity<>(result,HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    @RequestMapping(value = "/edit", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<?> updateGlAccount(@RequestBody GlAccount glAccount) throws Exception {
        try {
            Boolean result = glAccountService.updateGlAccount(glAccount);
            return new ResponseEntity<>(result,HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<?> deleteGlAccount(@PathVariable Long id) throws Exception {
        try {
            Boolean result = glAccountService.deleteGlAccount(id);
            return new ResponseEntity<>(result,HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<?> getGlAccount(@PathVariable Long id) throws Exception {
        GlAccount glAccount = null;
        try {
            glAccount = glAccountService.getGlAccountById(id);
            return new ResponseEntity<>(glAccount,HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    @RequestMapping(value = "/getall", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<?> getAllGlAccount() throws Exception {
        List<GlAccount> glAccounts = null;
        try {
            glAccounts = glAccountService.getAllGlAccounts();
            return new ResponseEntity<>(glAccounts,HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

}

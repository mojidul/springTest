package com.misl.umsys.chartofaccount;

import java.util.List;

/**
 * Created by Nayeem on 2/25/2018.
 */
public interface GlAccountService {

    Boolean createGlAccount(GlAccount glAccount) throws Exception;

    Boolean updateGlAccount(GlAccount glAccount) throws Exception;

    Boolean deleteGlAccount(long id) throws Exception;

    GlAccount getGlAccountById(long id) throws Exception;

    List<GlAccount> getAllGlAccounts() throws Exception;
}

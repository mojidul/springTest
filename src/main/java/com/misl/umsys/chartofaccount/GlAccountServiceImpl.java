package com.misl.umsys.chartofaccount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Nayeem on 2/25/2018.
 */
@Service
public class GlAccountServiceImpl implements GlAccountService {

    @Autowired
    private final GlAccountRepository glAccountRepository;

    @Autowired
    public GlAccountServiceImpl(GlAccountRepository glAccountRepository){
        this.glAccountRepository = glAccountRepository;
    }

    @Override
    public Boolean createGlAccount(GlAccount glAccount) throws Exception {

        try {
            GlAccount glAccount1 = this.getGlAccountById(glAccount.getId());
            if (glAccount1 == null){
                glAccountRepository.save(glAccount);
                return true;
            }else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public Boolean updateGlAccount(GlAccount glAccount) throws Exception {
        try {
            GlAccount glAccount1 = this.getGlAccountById(glAccount.getId());
            if (glAccount1 != null){
                glAccountRepository.save(glAccount);
                return true;
            }else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public Boolean deleteGlAccount(long id) throws Exception {
        try {
            GlAccount glAccount1 = this.getGlAccountById(id);
            if (glAccount1 != null){
                glAccountRepository.delete(id);
                return true;
            }else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public GlAccount getGlAccountById(long id) throws Exception {
        GlAccount glAccount = null;
        try {
            glAccount = glAccountRepository.findOne(id);
            return glAccount;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public List<GlAccount> getAllGlAccounts() throws Exception {
        List<GlAccount> glAccounts = null;
        try {
            glAccounts = glAccountRepository.findAll();
            return glAccounts;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
}

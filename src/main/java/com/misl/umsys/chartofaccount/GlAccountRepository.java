package com.misl.umsys.chartofaccount;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Nayeem on 2/25/2018.
 */
@Repository
public interface GlAccountRepository extends JpaRepository<GlAccount, Long>{
}

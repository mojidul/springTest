package com.misl.umsys.chartofaccount;

import javax.persistence.*;

/**
 * Created by Nayeem on 2/25/2018.
 */
@Entity
public class GlAccount {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;
    @Column(length = 40,nullable = false)
    private long glParentId;
    @Column(length = 20,nullable = false)
    private String glCode;
    @Column(length = 80)
    private String glDescription;
    @Column(length = 40,nullable = false)
    private long glId;
    @Column(length = 80)
    private String glMapCode;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getGlParentId() {
        return glParentId;
    }

    public void setGlParentId(long glParentId) {
        this.glParentId = glParentId;
    }

    public String getGlCode() {
        return glCode;
    }

    public void setGlCode(String glCode) {
        this.glCode = glCode;
    }

    public String getGlDescription() {
        return glDescription;
    }

    public void setGlDescription(String glDescription) {
        this.glDescription = glDescription;
    }

    public long getGlId() {
        return glId;
    }

    public void setGlId(long glId) {
        this.glId = glId;
    }

    public String getGlMapCode() {
        return glMapCode;
    }

    public void setGlMapCode(String glMapCode) {
        this.glMapCode = glMapCode;
    }
}

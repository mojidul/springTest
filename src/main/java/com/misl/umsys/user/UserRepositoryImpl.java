package com.misl.umsys.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Nayeem on 2/9/2018.
 */
@Repository
public interface UserRepositoryImpl extends JpaRepository<UserDetails,Long> {
    UserDetails findUserDetailsById(Long id);
}

package com.misl.umsys.user;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Nayeem on 2/9/2018.
 */
@Entity
@Table(name="USER_DETAILS")
public class UserDetails{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name="USER_ID")
    private String userName;
    private String firstName;
    @Transient
    private String middleName;
    private String lastName;
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;

//    @Embedded
//    @AttributeOverrides({
//            @AttributeOverride(name="village", column=@Column(name="Permanent_Village")),
//            @AttributeOverride(name="postOffice", column=@Column(name="Permanent_PostOffice")),
//            @AttributeOverride(name="thana", column=@Column(name="Permanent_Thana")),
//            @AttributeOverride(name="zilla", column=@Column(name="Permanent_Zilla")),
//            @AttributeOverride(name="country", column=@Column(name="Permanent_Country")),
//            @AttributeOverride(name="postCode", column=@Column(name="Permanent_PostCode"))
//    })
//    private Address permanentAddress;
//    @Embedded
//    private Address presentAddress;

    @ElementCollection
    private Set<Address> listOfAddress = new HashSet();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Set<Address> getListOfAddress() {
        return listOfAddress;
    }

    public void setListOfAddress(Set<Address> listOfAddress) {
        this.listOfAddress = listOfAddress;
    }
}

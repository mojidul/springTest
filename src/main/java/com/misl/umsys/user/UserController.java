package com.misl.umsys.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by Nayeem on 2/9/2018.
 */
@Controller
public class UserController {
    @Autowired
    UserServiceImpl userService;

    @RequestMapping(method= RequestMethod.GET,value="/users")
    public List<UserDetails> getAllSemister(){
        return userService.getAllUsers();
    }
    @RequestMapping(method= RequestMethod.GET,value="/users/{id}")
    public UserDetails getSemisterById(@PathVariable Long id){
        return userService.getUserById(id);
    }
    @RequestMapping(method= RequestMethod.POST,value="/users")
    public void addSemister(@RequestBody UserDetails semister){
        userService.addUser(semister);
    }

    @RequestMapping(method=RequestMethod.PUT,value="/users/{id}")
    public void updateSemister(@RequestBody UserDetails semister,@PathVariable Long id){
        userService.updateUser(id, semister);
    }

    @RequestMapping(method=RequestMethod.DELETE,value="/users/{id}")
    public void deleteSemister(@PathVariable Long id){
        userService.deleteUser(id);
    }
}

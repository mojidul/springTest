package com.misl.umsys.user;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Created by Nayeem on 2/9/2018.
 */
@Embeddable
public class Address {
    private String village;
    private String postOffice;
    private String thana;
    private String zilla;
    @Column(length = 50, nullable = false)
    private String country;
    private String postCode;

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getPostOffice() {
        return postOffice;
    }

    public void setPostOffice(String postOffice) {
        this.postOffice = postOffice;
    }

    public String getThana() {
        return thana;
    }

    public void setThana(String thana) {
        this.thana = thana;
    }

    public String getZilla() {
        return zilla;
    }

    public void setZilla(String zilla) {
        this.zilla = zilla;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }
}

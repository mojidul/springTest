package com.misl.umsys.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nayeem on 2/9/2018.
 */
@Service
public class UserServiceImpl {
    @Autowired
    UserRepositoryImpl userRepository;

    public List<UserDetails> getAllUsers(){
        List<UserDetails> userDetailses= new ArrayList<>();
        userRepository.findAll().forEach(userDetailses::add);
        return userDetailses;
    }

    public UserDetails getUserById(Long id){
        return userRepository.findOne(id);
    }

    public void addUser(UserDetails semister){
        userRepository.save(semister);
    }
    public void updateUser(Long id,UserDetails semister){
        userRepository.save(semister);
    }

    public void deleteUser(Long id){
        userRepository.delete(id);
    }
}

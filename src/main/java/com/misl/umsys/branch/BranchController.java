package com.misl.umsys.branch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.MediaType;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Created by Nayeem on 2/23/2018.
 */
@RestController
@RequestMapping("/branch")
public class BranchController {

    @Autowired
    private final BranchService branchService;

    @Autowired
    public BranchController(BranchService branchService){
        this.branchService = branchService;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<?> createBranch(@RequestBody Branch branch) throws Exception {
        try {
            Boolean result = branchService.createBranch(branch);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping("/get/{id}")
    public Branch getBranch(@PathVariable Long id) throws Exception {
        Branch branch = null;
        try {
            branch = branchService.getBranchById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return branch;
    }

}

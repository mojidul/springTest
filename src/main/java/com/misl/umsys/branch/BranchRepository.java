package com.misl.umsys.branch;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Nayeem on 2/23/2018.
 */
@Repository
public interface BranchRepository extends JpaRepository<Branch, Long> {
}

package com.misl.umsys.branch;

/**
 * Created by Nayeem on 2/23/2018.
 */
public interface BranchService {
    Boolean createBranch(Branch branch) throws Exception;

    Branch getBranchById(Long id) throws Exception;
}

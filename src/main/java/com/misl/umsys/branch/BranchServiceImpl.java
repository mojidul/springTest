package com.misl.umsys.branch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Nayeem on 2/23/2018.
 */
@Service
public class BranchServiceImpl implements BranchService{

    @Autowired
    private final BranchRepository branchRepository;

    @Autowired
    public BranchServiceImpl(BranchRepository branchRepository){
        this.branchRepository = branchRepository;
    }

    @Override
    public Boolean createBranch(Branch branch) throws Exception {
        try {
            branchRepository.save(branch);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public Branch getBranchById(Long id) throws Exception {

        Branch branch = null;
        try {
            branch = branchRepository.findOne(id);
            return branch;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

}

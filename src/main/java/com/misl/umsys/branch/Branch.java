package com.misl.umsys.branch;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Nayeem on 2/23/2018.
 */
@Entity
public class Branch implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;
    private String name;
    private String routingNumber;
    private String swiftCode;
    private boolean ownBank;

    @Embedded
    private Coordinates location;

    @ElementCollection
    @CollectionTable(name = "PHONES", joinColumns = @JoinColumn(name = "id", referencedColumnName = "id"))
    @Column(name = "phone")
    private List<String> phones;

    @ElementCollection
    @CollectionTable(name = "EMAILS", joinColumns = @JoinColumn(name = "id", referencedColumnName = "id"))
    @Column(name = "email")
    private List<String> emails;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoutingNumber() {
        return routingNumber;
    }

    public void setRoutingNumber(String routingNumber) {
        this.routingNumber = routingNumber;
    }

    public String getSwiftCode() {
        return swiftCode;
    }

    public void setSwiftCode(String swiftCode) {
        this.swiftCode = swiftCode;
    }

    public boolean isOwnBank() {
        return ownBank;
    }

    public void setOwnBank(boolean ownBank) {
        this.ownBank = ownBank;
    }

    public Coordinates getLocation() {
        return location;
    }

    public void setLocation(Coordinates location) {
        this.location = location;
    }

    public List<String> getPhones() {
        return phones;
    }

    public void setPhones(List<String> phones) {
        this.phones = phones;
    }

    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }
}

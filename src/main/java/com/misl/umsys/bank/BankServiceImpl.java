package com.misl.umsys.bank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by user on 5/31/2018.
 */
@Service
public class BankServiceImpl implements BankService {

    private final BankRepository bankRepository;

    @Autowired
    public BankServiceImpl(BankRepository bankRepository){
        this.bankRepository = bankRepository;
    }
    @Override
    public List<Bank> getAlBanks() throws Exception {
        List<Bank> banks = null;
        try {
            banks = bankRepository.findAll();
            return banks;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
}

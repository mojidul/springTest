package com.misl.umsys.bank;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by user on 5/31/2018.
 */
@Repository
public interface BankRepository extends JpaRepository<Bank, Long>{
}

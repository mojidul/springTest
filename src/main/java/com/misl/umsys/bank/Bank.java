package com.misl.umsys.bank;

import com.misl.umsys.branch.Branch;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

/**
 * Created by user on 5/31/2018.
 */
@Entity
public class Bank {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long bankId;
    @Column(length = 20,nullable = false)
    private String bankCode;
    @Column(length = 80)
    private String bankName;
//    @OneToMany(mappedBy = "bank")
//    @Fetch(FetchMode.SUBSELECT)
//    private List<Branch> branches;

    public long getBankId() {
        return bankId;
    }

    public void setBankId(long bankId) {
        this.bankId = bankId;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
}

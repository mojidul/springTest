package com.misl.umsys.bank;

import java.util.List;

/**
 * Created by user on 5/31/2018.
 */
public interface BankService {
    List<Bank> getAlBanks() throws Exception;
}

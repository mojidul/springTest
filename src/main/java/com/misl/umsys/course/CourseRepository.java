package com.misl.umsys.course;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface CourseRepository extends CrudRepository<Course, Integer> {
	Course findById(int id);

	List<Course> findByDepartmentId(int departmetnId);

}

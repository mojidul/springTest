// Defining angularjs application.
    var postApp = angular.module('postApp', []);
    // Controller function and passing $http service and $scope var.
    postApp.controller('postController', function($scope, $http) {
      // create a blank object to handle form data.
        $scope.department = {};
      // calling our submit function.
        $scope.submitForm = function() {
        // Posting data to java file
        $http({
          method  : 'POST',
          url     : '/uni-man-sys/departments',
          data    : $scope.department, //forms user object
          headers : {'Content-Type': 'application/json'} 
         })
          .success(function(data) {
            if (data.errors) {
              // Showing errors.
              $scope.errorCode = data.errors.deptCode;
              $scope.errorName = data.errors.deptName;
              } else {
              $scope.message = data.message;
              $scope.department=null;
            }
          });
        };
    });
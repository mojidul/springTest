// Defining angularjs application.
var postApp = angular.module('courseApp', [ 'ngRoute' ]);
// Controller function and passing $http service and $scope var.
postApp.controller('courseController', function($scope, $http) {
	$http({
		method : 'GET',
		/*url : 'http://localhost:8080/uni-man-sys/departments'*/
		url : '/uni-man-sys/departments'
		
	}).success(function(data, status, headers, config) {
		$scope.departments = data;
	}).error(function(data, status, headers, config) {
		alert('error');
	});
	
	$http({
		method : 'GET',
		/*url : 'http://localhost:8080/uni-man-sys/departments'*/
		url : '/uni-man-sys/semisters'
		
	}).success(function(data, status, headers, config) {
		$scope.semisters = data;
	}).error(function(data, status, headers, config) {
		alert('error');
	});
	
// submit data course
	
	// calling our submit function.		
		
    $scope.courseSave = function() {
    	

    	$scope.course = {};
    	//var departmentId = {param:'departmentId',param:'courses'}
    	//$scope.departmentId=departmentId;
    	//$scope.semisterId=semisterId;
    	//$scope.url = '/uni-man-sys/departments/' + $scope.departmentId + '/' + $scope.course;
    	
    // Posting data to java file
    	$http({
    		method  : 'POST',
    		//url     : '/uni-man-sys/departments/departmentId/courses',
    		//url     : $scope.url,
    		url	: '/departments/{departmentId}/courses/semisters/course.semisterId',
    		data    : $scope.course, 
    		headers : {'Content-Type': 'application/json'}
    		//params  : departmentId	  
    		})
    			.success(function(data) {
    				if (data.errors) {
    					// Showing errors.
    					$scope.errorCode = data.errors.code;
    					$scope.errorName = data.errors.name;
    					$scope.errorName = data.errors.credit;
									} else {
										$scope.message = data.message;
										$scope.course=null;
											}
										});
									};
	

});